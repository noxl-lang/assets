# Noxl Programming Language Assets

This repository holds all of the assets for the Noxl programming language.
This is a seperate repository, because the assets are under a different open source license.

> By the way, the logo uses the Everforest Medium Dark color scheme.

![Noxl Logo](https://gitlab.com/noxl-lang/assets/-/raw/main/logo/logo.svg)
